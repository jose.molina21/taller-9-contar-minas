class Sembrador {
    //atributos
    campo;
    campoCantidadMinas;
    tamaño;
    cantidadMinas;

    constructor() {
        this.tamaño = 10;
        this.campo = Array();
        this.cantidadMinas = Math.round(Math.pow(this.tamaño, 2) * 0.1);
    }

    //creacion de matriz

    crearCampo() {
        for (let i = 0; i < this.tamaño; i++) {
            for (let j = 0; j < this.tamaño; j++) {
                if (!this.campo[i]) {
                    this.campo[i] = Array();
                }
                this.campo[i][j] = 0;
            }

        }
    }

    //sembrador de minas

    sembrarMinas() {

        while (this.cantidadMinas > 0) {
            let fila = Math.floor(Math.random() * this.tamaño);
            let columna = Math.floor(Math.random() * this.tamaño);

            if (this.campo[fila][columna] === 0) {
                this.campo[fila][columna] = "b";
                this.cantidadMinas--;

            }
        }
    }

    //contar minas alrededor d cada casilla

    contarMinasAlrededor() {
        for (let i = 0; i < this.tamaño; i++) {
            for (let j = 0; j < this.tamaño; j++) {
                if (this.campo[i][j] != "b") {
                    let contar = 0;
                    for (let k = i - 1; k <= i + 1; k++) {
                        for (let l = j - 1; l <= j; l++) {
                            if (k > -1 && k < this.tamaño && l > -1 && l < this.tamaño) {
                                if (this.campo[k][l] == "b") {
                                    contar++;
                                }
                            }
                        }
                    }
                    this.campo[i][j] = contar;
                }
            }
        }

    }

    set tamañoUsuario(valor) {
        if (valor < 4) {
            this.tamaño = 4;
        } else {
            this.tamaño = valor;
        }
    }

    set cantidadMinasUsuario(valor) {
        valor = Math.round(valor);

        if (valor > 0.2 * Math.pow(this.tamaño, 2)) {
            valor = Math.round(Math.pow(this.tamaño, 2) * 0.1);
        }

        if (valor < 1) {
            this.cantidadMinas = 2;
        } else {
            this.cantidadMinas = valor;
        }
    }
}

let miSembrador = new Sembrador();

miSembrador.tamañoUsuario = 10;
miSembrador.cantidadMinasUsuario = 20;
miSembrador.crearCampo();
miSembrador.sembrarMinas();
miSembrador.contarMinasAlrededor();